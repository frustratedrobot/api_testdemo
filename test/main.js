const request = require('supertest')('https://jsonplaceholder.typicode.com');
const assert = require('assert');

describe('GET /todos/1', () => {
  it('should work for /todos', async () => {
    const response = await request
			.get('/todos/1')
			.expect(200);
			assert.equal(response.body.userId, '1');
			assert.equal(response.body.id, '1');
			assert.equal(response.body.completed, false);
  });
});

describe('GET /users', () => {
  it('should work for /users', async () => {
    const response = await request
			.get('/users')
			.expect(200);
			assert.equal(response.body[0].name, 'Leanne Graham');
			assert.equal(response.body[0].username, 'Bret');
			assert.equal(response.body[0].website, 'hildegard.org');
  });
});
