FROM node:latest

RUN mkdir /src
WORKDIR /src

ADD package.json /src/package.json
ADD test/main.js /src/test/main.js

RUN npm install

CMD npm run test
